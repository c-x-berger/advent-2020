#![feature(str_split_once)]
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Result as ioResult;

use itertools::Itertools;

const VALID_ECL: [&'static str; 7] = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];

fn is_valid_passport(s: &str) -> bool {
    let mut result: u8 = 0;
    for field in s.split_whitespace() {
        let kv = field.split_once(":").unwrap();
        match kv.0 {
            "byr" => {
                let byr = kv.1.parse().unwrap_or(0);
                result |= if (1920..=2002).contains(&byr) { 1 } else { 0 };
            }
            "iyr" => {
                let iyr = kv.1.parse().unwrap_or(0);
                result |= if (2010..=2020).contains(&iyr) { 2 } else { 0 };
            }
            "eyr" => {
                let eyr = kv.1.parse().unwrap_or(0);
                result |= if (2020..=2030).contains(&eyr) { 4 } else { 0 };
            }
            "hgt" => {
                if let Some(h) = kv.1.strip_suffix("cm") {
                    if (150..=193).contains(&h.parse().unwrap_or(0)) {
                        result |= 8;
                    }
                } else if let Some(h) = kv.1.strip_suffix("in") {
                    if (59..=76).contains(&h.parse().unwrap_or(0)) {
                        result |= 8;
                    }
                }
            }
            "hcl" => {
                if let Some(color) = kv.1.strip_prefix('#') {
                    if color.len() == 6
                        && color
                            .bytes()
                            .all(|b| (48..=57).contains(&b) || (97..=102).contains(&b))
                    {
                        result |= 16;
                    }
                }
            }
            "ecl" => {
                result |= if VALID_ECL.contains(&kv.1) { 32 } else { 0 };
            }
            "pid" => {
                if kv.1.len() == 9 && kv.1.parse::<usize>().is_ok() {
                    result |= 64;
                }
            }
            _ => {}
        }
    }
    result == 127
}

fn main() -> ioResult<()> {
    let input = File::open("input.txt")?;
    let reader = BufReader::new(input);
    let groups = reader.lines().map(|r| r.unwrap()).group_by(|l| l != "");
    let mut valid_passports = 0;
    for group in groups.into_iter().filter_map(|g| {
        if g.0 {
            return Some(g.1);
        }
        None
    }) {
        let passport = group.collect::<Vec<_>>().join(" ");
        let valid = is_valid_passport(&passport);
        if valid {
            valid_passports += 1;
        }
        println!("Passport: {}\nValid: {}\n===========", passport, valid);
    }
    println!("total valid passports: {}", valid_passports);
    Ok(())
}
