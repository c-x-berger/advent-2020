use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Result;

const TREE: char = '#';

const SLOPES: [(usize, usize); 5] = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];

fn main() -> Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let mut forest = vec![];
    for line in reader.lines().map(|r| r.unwrap()) {
        forest.push(line);
    }
    let mut result: usize = 1;
    for slope in &SLOPES {
        let mut position = slope.0;
        let mut trees = 0;
        for row in forest[slope.1..].iter().step_by(slope.1) {
            let ch_at = row.chars().nth(position);
            if ch_at.unwrap() == TREE {
                trees += 1;
            }
            position += slope.0;
            position = position % row.len();
        }
        println!(
            "going at slope ({}, {}), you hit {} trees",
            slope.0, slope.1, trees
        );
        result *= trees;
    }
    println!("product of all whacks: {}", result);
    Ok(())
}
