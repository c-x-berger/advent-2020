use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> std::io::Result<()> {
    let file = File::open("input.txt")?;
    let reader = BufReader::new(file);
    let lines = reader.lines().map(|r| r.unwrap());
    let ints: Vec<_> = lines.map(|l| l.parse::<usize>().unwrap()).collect();
    for (i, iv) in ints.iter().enumerate() {
        for (j, jv) in ints[i + 1..].iter().enumerate() {
            for (k, kv) in ints[j + 1..].iter().enumerate() {
                if iv + jv + kv == 2020 {
                    println!("match, i * j = {}", iv * jv * kv);
                    return Ok(());
                }
            }
        }
    }
    Ok(())
}
