#![feature(str_split_once)]
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Result;

#[derive(Default)]
struct Policy {
    min: usize,
    max: usize,
    ch: char,
}

impl Policy {
    fn new(min: usize, max: usize, ch: char) -> Self {
        Self { min, max, ch }
    }

    fn is_password_valid(&self, password: &str) -> bool {
        // let count = password.chars().filter(|c| *c == self.ch).count();
        // (self.min..=self.max).contains(&count)
        return (password.chars().nth(self.min - 1) == Some(self.ch))
            ^ (password.chars().nth(self.max - 1) == Some(self.ch));
    }
}

fn main() -> Result<()> {
    let file = File::open("input.txt")?;
    let lines = BufReader::new(file).lines();
    let mut count = 0;
    for line in lines.map(|r| r.unwrap()) {
        // X-Y c: password
        let pair = line.split_once(":").unwrap();
        let policy = pair.0.split_once(" ").unwrap();
        let password = pair.1.trim();
        let ch = policy.1.chars().nth(0).unwrap();
        let range = policy.0.split_once("-").unwrap();
        let policy = Policy::new(range.0.parse().unwrap(), range.1.parse().unwrap(), ch);
        if policy.is_password_valid(&password) {
            count = count + 1;
        }
    }
    println!("there are {} valid passwords", count);
    Ok(())
}
